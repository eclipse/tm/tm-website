---
title: "About Target Management"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---

About Target Management
=======================

Mission Statement
-----------------

The Target Management project creates data models and frameworks to configure and manage remote systems, their connections, and their services.

*   _Remote systems_ (aka targets) can range from large-scale host computers, multi-board or multi-core parallel systems to target boards without even a CPU or FPGA (connected by a hardware debugging box).
*   _Configuration_ means dealing with board description files from a variety of sources, displaying remote system components and manipulating board and connection properties.
*   _Management_ means maintaining a list of remote systems which is team shareable and available to other subsystems, creating groups of systems when appropriate, and handling access control to targets that are shared in a team (board lab).
*   _Connections_ can range from TCP/IP standard protocols like FTP or telnet, secure encryption, authentication and firewall tunnelling (ssh), to serial line terminal and vendor-specific protocols for remote agent and on-chip debugging (JTAG) solutions. We want connection schemes and protocols to be pluggable in our framework.
*   _Services_ on remote systems include downloading software and data, launching single or multiple configurations, starting and stopping cores, resetting and querying target information.

Since there are many different vendors and solutions around in the device software space, the main charter of target management is to provide data models and frameworks that are flexible and open enough for vendor-specific extensions. Sample implementations are provided for TCP/IP Secure Shell (ssh) connections, FTP data transfer, Telnet and Windows CE connections. In addition to that, a lightweight Terminal view is provided as a standalone offering.

Current releases can be downloaded for evaluation [here](/tm/downloads).

### For more information, see the

*   [Target Management Overview Slides](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.pdf) which include a diagram of the envisioned components and architecture for our project ([PPT](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.ppt) | [PDF](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.pdf)).
*   [TM Webinar](http://live.eclipse.org/node/229): goals, architecture, future plans and online demo ([50 minute full recording](http://live.eclipse.org/node/229) | [PPT slides](http://www.eclipse.org/projects/slides/TM_Webinar_Slides_070412.ppt))
*   [Target Management Use-Case Document](/tm/doc/TM_Use_Cases_v1.1c.pdf) to understand what scenarios we want to cover with our project.
*   [Target Management Project Planning](/tm/development/plan) to understand what features and releases are coming next.
