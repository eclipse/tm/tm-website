---
title: "TM Repository and Team Project Sets"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---

TM Repository and Team Project Sets
===================================

When you want to work on RSE code obtained from our Git Repositories, the simplest way will be to set up your workspace using our team project sets. These are currently being constructed for our Git repository and will be available from this page.

For more details on Team Project Sets and Git, see the [Team Project Set Support](http://wiki.eclipse.org/EGit/New_and_Noteworthy/1.1#Team_Project_Set_Support) of the [EGit 1.1 New and Noteworthy](http://wiki.eclipse.org/EGit/New_and_Noteworthy/1.1) document.

Direct access to the TM repository
----------------------------------

Our repository can be viewed at [http://git.eclipse.org/c/tm/org.eclipse.tm.git/](http://git.eclipse.org/c/tm/org.eclipse.tm.git/). There you will also find the URLs for cloning the repository if you desire to do so.

Background information on using Git with Eclipse
------------------------------------------------

*   See [Git](http://wiki.eclipse.org/Git) for information on how to use Eclipse with Git.
*   See [the EGit FAQ](https://github.com/eclipse-egit/egit/wiki/FAQ) for information on how to use EGit, the Eclipse Git client.
