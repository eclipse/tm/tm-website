---
title: "Target Management Project Planning"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---

Target Management Project Planning
==================================

### Releases currently under development

*   [Target Management Project 3.5 Plan](http://projects.eclipse.org/projects/tools.tm/releases/3.5.0)  
    This document lays out the feature and API set for the Target Management 3.5 release.
*   [Target Management Bug Process](/tm/development/bug_process)  
    Queries for up-to-date status on plan items and bugs from Eclipse Bugzilla.
*   [TM Future Planning Wiki](https://gitlab.eclipse.org/groups/eclipse/tm/-/wikis/TM_Future_Planning)  
    Plan Incubator Wiki, holding ideas for future releases.
*   [TM Wiki](https://gitlab.eclipse.org/groups/eclipse/tm/-/wikis/TM)  
    Target Management Wiki, linking to Technology Sub-Groups for breeding future ideas.

### Development Process

*   [TM Development Home Page](/tm/development/)  
    Main page for Target Management Development Resources.
*   [Target Management Use Cases Document](/tm/doc/TM_Use_Cases_v1.1c.pdf)  
    Requirements: Target Management Use Cases - dated but still current.
*   [Developer Documents](/tm/doc/)  
    Target Management Development Documents.

### Historical information about past releases

*   [Target Management Project 3.4 Plan](http://www.eclipse.org/projects/project-plan.php?planurl=http://www.eclipse.org/tm/development/tm_plan_3_4.xml)  
    This document lays out the feature and API set for the Target Management 3.4 release.
*   [Target Management Project 3.3 Plan](http://www.eclipse.org/projects/project-plan.php?planurl=http://www.eclipse.org/tm/development/tm_plan_3_3.xml)  
    This document lays out the feature and API set for the Target Management 3.3 release.
*   [Target Management Project 3.2 Plan](http://www.eclipse.org/projects/project-plan.php?planurl=http://www.eclipse.org/tm/development/tm_plan_3_2.xml)  
    This document lays out the feature and API set for the Target Management 3.2 release.
*   [Target Management Project 3.1 Plan](http://www.eclipse.org/projects/project-plan.php?planurl=http://www.eclipse.org/tm/development/tm_plan_3_1.xml)  
    This document lays out the feature and API set for the Target Management 3.1 release.
*   [Target Management Project 3.0 Plan](tm_project_plan_3_0.html)  
    This document lays out the feature and API set for the Target Management 3.0 release.
*   [Target Management Project 2.0 Plan](tm_project_plan_2_0.html)  
    This document lays out the feature and API set for the Target Management 2.0 release.
*   [Target Management Project 1.0 Plan](tm_project_plan_1_0.html)  
    This document lays out the feature and API set for the Target Management 1.0 release.
