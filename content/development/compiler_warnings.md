---
title: "Recommended Compiler Warnings"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---

Recommended Compiler Warnings
=============================

For the Target Management Project, we recommend enabling the following compiler warnings in JDT (_Window > Preferences > Java > Compiler > **Errors/Warnings** and **Javadoc**_). Settings that differ from the JDT default are marked in bold.

In most cases, we use stricter warnings in the TM project than the JDT default suggests - with one notable exception: Because parts of RSE require access to Platform "internal" packages, and this requirement cannot be fixed currently, we disable the "discouraged access" warning for now.

| Setting | Value | Comment |
| ------- | ----- | ------- |
| **Code Style** | | |
| Non-static access to static member: | Warning | |
| Indirect access to static member: | **Warning** | |
| Unqualified access to instance field: | Ignore | |
| Undocumented empty block: | Ignore | |
| Access to a non-accessible member of an enclosing type: | Ignore |  |
| Method with a constructor name: | **Error** | |
| Parameter assignment: | Ignore | |
| Non-externalized strings (missing/unused -NLS$ tag): | **Warning** | |
| **Potential programming problems** | | |
| Serializable class without serialVersionUID: | Warning | |
| Assignment has no effect (e.g. 'x = x'): | Warning | |
| Possible accidental boolean assignment (e.g. if (a=b)): | **Warning** | |
| 'finally' does not complete normally: | Warning | |
| Empty statement: | **Warning** | |
| Using a char array in string concatenation: | Warning | |
| Hidden catch block: | Warning | |
| Inexact type match for vararg arguments: | Warning | |
| Boxing and unboxing conversions: | **Warning** | |
| Enum type constant not covered on 'switch': | **Warning** | |
| 'switch' case fall-through: | **Warning** | |
| Null reference: | **Warning** | |
| **Name shadowing and conflicts** | | |
| Field declaration hides another field or variable: | **Warning** | |
| Local variable declaration hides another field or variable: | Ignore | |
|    Include constructor or setter method parameters | | |
| Type parameter hides another type: | Warning | |
| Method overridden but not package visible: | **Error** | |
| Interface method conflicts with protected 'Object' method: | Warning | |
| **Deprecated and restricted API** | | |
| Deprecated API: | Warning | |
|    Signal use of deprecated API inside deprecated code | **On** | |
|    Signal overriding or implementing deprecated method | **On** | |
| Forbidden reference (access rules): | Error | |
| Discouraged reference (access rules): | Warning | |
| **Unnecessary code** | | |
| Local variable is never read: | Warning | |
| Parameter is never read: | Ignore | |
|    Check overriding and implementing methods | | |
| Unused import:: | Warning | |
| Unused local or private member:: | Warning | |
| Unnecessary else statement:: | Ignore | |
| Unnecessary cast or 'instanceof' operation:: | **Warning** | |
| Unnecessary declaration of thrown checked exception:: | Ignore | |
|    Check overriding and implementing methods | | |
| Unused break/continue label:: | Warning | |
| **Generic types** | | |
| (Not applicable since TM does not use Java 5) | | |
| **Annotations** | | |
| (Not applicable since TM does not use Java 5) | | |
| **Javadoc** | | |
| Process Javadoc comments (includes search and refactoring) | On | |
| Malformed Javadoc comments: | **Warning** | |
|    Only consider members visible as: | **Protected** | |
|    Report errors in tags | **On** | |
|    Report non visible references | **On** | |
|    Report deprecated references | **On** | |
| Missing Javadoc tags: | Ignore | |
| Missing Javadoc comments: | Ignore | |