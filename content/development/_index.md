---
title: "TM Developer Resources"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---

TM Developer Resources
======================

### Development Tools

*   [**CVS Repository**]({{< ref "cvs_setup" >}})
    TM development is managed in the [RSE](http://dev.eclipse.org/viewcvs/index.cgi/org.eclipse.tm.rse/?cvsroot=Tools_Project) and [Core](http://dev.eclipse.org/viewcvs/index.cgi/org.eclipse.tm.core/?cvsroot=Tools_Project) Repositories. To populate an Eclipse workspace with our code, best use our [Team Project Sets]({{< ref "cvs_setup" >}}). A [CVS Changelog](http://download.eclipse.org/tm/downloads/drops/N-changelog/index.html) is available, and you can subscribe to commit notifications on [tm-cvs-commit](https://dev.eclipse.org/mailman/listinfo/tm-cvs-commit).
*   [**Bug Process**]({{< ref "bug_process" >}})  
    View [all open](https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&product=Target+Management&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&cmdtype=doit) issues | [Submit new](https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Target%20Management&version=unspecified&component=RSE) bugs | Request an [enhancement](https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Target%20Management&version=unspecified&component=RSE&rep_platform=All&op_sys=All&priority=P3&bug_severity=enhancement&form_name=enter_bug)
*   **[Eclipse API Scanner](http://www.eclipse.org/webtools/development/apiscanner/apiscanner.html)**  
    Describe the API of a component and scan plug-ins for API violations (from the [WebTools](http://www.eclipse.org/webtools) project).
*   **[Check Unused Properties Tool](http://www.eclipse.org/webtools/development/piitools/piitools.html)**  
    Scan property files for unused messages (from the [WebTools](http://www.eclipse.org/webtools) project).
*   **[Core Tools](http://www.eclipse.org/eclipse/platform-core/downloads.html)**  
    Useful utilities from the Platform team. Consider adding their [update site](http://www.eclipse.org/eclipse/platform-core/downloads.html#updates) to your configuration.
*   **[Building RSE](http://dev.eclipse.org/viewcvs/index.cgi/org.eclipse.tm.rse/releng/org.eclipse.rse.build/README_build.txt?revision=1.2&root=Tools_Project)**  
    Instructions how to perform unattended builds of TM / RSE.
*   **[Release Engineering](http://wiki.eclipse.org/Platform-releng)**  
    See the instructions, FAQs and help from the Platform-Releng team.
*   **[Eclipse Committer Tools](https://dev.eclipse.org/committers)**  
    Information on infrastructure status, file path information, FAQs, etc. \[login required\]

### Development Resources

*   **[Committer HOWTO](/tm/development/committer_howto)**  
    Simple cookbook instructions for performing basic tasks properly. Committers need to follow check-in conventions and IP Due Diligence as outlined [here](/tm/development/committer_guide).
*   **[API Guidelines](https://github.com/eclipse-platform/eclipse.platform/blob/master/docs/API_Central.md)**  
    Look [here](http://help.eclipse.org/help30/topic/org.eclipse.platform.doc.isv/reference/misc/api-usage-rules.html) for general Eclipse API rules of engagement. We strive to achieve [Eclipse Quality](http://www.eclipse.org/projects/dev_process/eclipse-quality.php), as outlined in the draft of [Eclipse Quality APIs](http://www.eclipse.org/projects/dev_process/Eclipse%20Quality%20APIs%20v2.pdf).
*   **[Development Conventions and Guidelines](http://wiki.eclipse.org/Development_Conventions_and_Guidelines)**  
    Coding standards, naming conventions, and other guidelines used by the Platform. TM will use these conventions until such time as deviation is required.
*   **[Architectural Overview](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.pdf)**  
    A high level summary of the components and their architecture.
*   **[Project Requirements](http://www.eclipse.org/tm/doc/TM_Use_Cases_v1.1c.pdf)**  
    Look here for a list of the project use cases and their priorities.
*   **[Charter](http://www.eclipse.org/tools/eclipsetools-charter.php)**  
    As a Tools subproject, the TM project abides by the [Tools Project charter](/tools/eclipsetools-charter.php) and the [Eclipse Standard Charter](http://www.eclipse.org/projects/dev_process/Eclipse_Standard_TopLevel_Charter_v1.0.php).
*   **[Contributors and Committers](/tm/development/contributors)**  
    List of TM Project contributors and committers.
