---
title: "TM Committer Howto"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---

TM Committer Howto
==================

While we do not want to get too process-heavy, there are a few simple things that will make all our lives easier. Here are a few "howto..." cookbook-style instructions for performing typical committer tasks.

If you need any background information regarding these instructions, look at the [committer guidelines](/tm/development/committer_guide).

How do I...

*   [Properly check my code before checkin](#check_code)
*   [Format my checkin comment](#format_checkin_comment)
*   [Work with bugs and fixes](#checkin_fix)
*   [Apply a patch from an external contributor](#external_contrib)
*   [Add some legacy code to the project](#legacy_code)
*   [Count lines of code in a contribution](#sloccount)
*   [Add a plugin or other project to the workspace](#add_plugin)
*   [Find missing or incorrect Copyright notices in my code and fix them](#fix_copyrights)

Properly check my code before checkin
-------------------------------------

*   Ensure that you have [Compiler Warnings](/tm/development/compiler_warnings) switched on as recommended. For every file that you modify, please ensure that it compiles without warnings before you check it in.
*   Keep the [Code Ownership Page](https://gitlab.eclipse.org/groups/eclipse/tm/-/wikis/Code_Ownership) up-to-date on the Wiki.
*   Ideally, run [FindBugs on your code to find additional issues.](http://www.jroller.com/andyl/entry/findbugs_update1)

Format my checkin comment
-------------------------

All checkin comments should be formatted like when copy-and-pasting the headline of a Bugzilla item, for example:

		Bug 338635 - [releng] Retire or migrate the dsdp.eclipse.org vserver

related to [bug 338635](https://bugs.eclipse.org/bugs/show_bug.cgi?id=338635)

Work with bugs and fixes
------------------------

*   See the [Bug Process Page](/tm/development/bug_process) for bugzilla queries to find interesting bugs, and our general bug process (handling bugzilla states and priorities)
*   Test your fix once to verify that it actually works - if this is not too difficult. You may also place a note on the bugzilla report, asking the original submitter to verify.
*   Ideally, write a JUnit test to make sure the bug will not creep in again (may not always be applicable).
*   Commit your fix, [adding the bugzilla number](#format_checkin_comment) to the commit message. Example:
    *   **\[139207\] fix browsing into tar archives by dstore**
*   Set the **bugzilla report FIXED** and verify that the bugzilla **Target Milestone** reflects the version that your fix is about to go into. The bugzilla comment along with the state change should include some help that allows a reader to understand what was done. Some good examples:
    
    *   **Fixed by correcting id in files.ui/plugin.xml**
    *   **Fixed in SystemViewPart and other \*ViewPart files by using common action handler**
    
    The comment in bugzilla should be short enough so that it is not much effort creating it. But long enough to help a later reader find the corresponding change in cvs, and/or get some idea what the problem actually was and what was done about it:
    *   In case a single file was changed, a reference to the filename is sufficient
    *   In case multiple files were changed, the most dominant file is often sufficient
    *   In case lots of files were changed, the plugin or the component is fine

Apply a patch from an external contributor
------------------------------------------

*   Ensure that the patch is on **bugzilla**. If not, ask the contributor to create a bugzilla entry. This is required even for patches from co-workers in your own company!
*   **Contact the contributor** and ask the following (see the [Eclipse Legal Poster](http://www.eclipse.org/legal/EclipseLegalProcessPoster.pdf)):
    
    *   If this is the first time the contributor makes a contribution: Contact information for IP records - Name, Company, E-Mail, Office address, Office phone.
    *   Did you write the contribution yourself?
    *   Did you reference any 3rd party material?
    *   If yes, under what terms (license) did you receive this material?
    *   Are you authorized by your employer to make the contribution?
    
    In the typical case (no IP problems expected), the contributor should put a message like the following on the bugzilla report that holds his/her patch:
    
    Legal Message: I, {name}, declare that I developed attached code from scratch, without referencing any 3rd party materials except material licensed under the EPL. {I am authorized by my employer to make this contribution under the EPL.}
    
    The point about _"...authorized by my employer"_ must be added when the contribution is made as part of an employee relationship that makes the employer the actual copyright owner. In case the contribution is written as a private activity, the following should be added instead:
    
    I certify that I am the copyright owner and authorize this contribution.
    
    For more info, see the [EPL FAQ](http://www.eclipse.org/legal/eplfaq.php).
*   Apply the patch in a **test workspace**, and review the code for any obvious traces of copyright breaches, blasphemy or "bad words". It makes sense to have a separate workspace for applying patches only, such that your current work on other things is not affected negatively.
*   **Collaborate with the contributor** (i.e. exchange e-mails) until the patch satisfies your needs. Doing so educates our contributors, keeps the process transparent, and takes work off committers:
    *   Patch works on HEAD (if not: ask contributor to re-base and re-submit the patch)
    *   Contributor''s name added to the Copyright section of all modified files
    *   Coding style appropriate
    .
*   If the contribution is more than 250 lines of code and the contributor is not from your own company, or there is any uncertainty about licensing and purity of IP and copyrights: fill out a Contribution Questionnaire (available from the [Portal](http://portal.eclipse.org))
*   Once IP due diligence is completed: Apply the patch and commit it. Do not make local modifications between applying and committing, in order to keep the process transparent. **Put the bugzilla number on the commit message**.
*   Verify that the bugzilla **Target Milestone** reflects the version that your fix is about to go into, then set the bugzilla report to **FIXED**.
*   Add the bugzilla **iplog+** flag **On the attachment** that was committed to CVS and that came from the original contributor. This is important in order to properly generate the [Automatic IP Log](http://wiki.eclipse.org/Development_Resources/Automatic_IP_Log).

Add some legacy code to the project
-----------------------------------

*   Obtain **PMC Member Approval**: Write an e-mail to tools-pmc@eclipse.org, describing the intended contribution. Give the PMC an idea of how large the contribution is, what it is good for (cryptography?), and if there are any other licenses than the EPL involved.
*   PMC Member Approval should be returned by E-Mail. You don't need to wait for the full PMC approval since it will be tracked via the CQ you are going to create -- just wait long enough to be reasonably sure that the PMC is not going to deny the request (because then you'd be creating the CQ in vain).
*   Create a **Bugzilla Entry** holding the contribution in a form that is suitable for checkin. A ZIP archive of all the files/projects affected is fine.
*   Fill in a Contribution Questionnaire (available from the [Portal](http://portal.eclipse.org)) and wait for EMO approval. You'll need to attach the contribution's source code once again, but creating the CQ is effortless based on the bugzilla ID you already have.
*   Once approved, check in the contribution. Dont forget to add the **bugzilla number on the commit message.** Commit the code verbatim as from the bug entry first, and make any necessary modifications later.
*   If project(s) were added, update the **\*.psf Project Set Files** (see [below](#add_plugin)).

Count lines of code in a contribution
-------------------------------------

*   For Eclipse IP review bookkeeping, we need to count lines of code in contribution, INCLUDING xml files, documentation, readmes, property files and so on. Most known code counters do not fulfill these needs. The simple shellscripts below count all lines of all text files except empty lines.
*   We optionally also suppress lines that only contain whitespace, empty comments or {} charactes. This is in order to account for different coding styles, which might prefer to have the \{ characters on a separate line or not.
*   ![](http://www.eclipse.org/images/new.gif) We now have scripts to count the lines of code in a contribution. Download any of the following:
    
    *   [line\_count\_scripts-2.0RC1.tar](http://www.eclipse.org/downloads/download.php?file=/tm/div/line_count_scripts-2.0RC1.tar)
    *   [line\_count\_scripts-2.0RC1.zip](http://www.eclipse.org/downloads/download.php?file=/tm/div/line_count_scripts-2.0RC1.zip)
    
    These scripts count all lines in either a patch or a list of directories, suppressing binary files and optionally also empty lines or lines containing only empty comments. See the script source code for details.
    *   UNIX shellscripts should run out of the box. On Windows, Cygwin is required. Windows .bat files are provided as wrappers if cygwin is not your primary environment. The .bat files expect the cygwin bin directory in your PATH or installed in C:\\Cygwin\\bin - change the .bat file if you do not like this assumption.
    *   For contributions supplied as an archive, use the [lc script](http://dev.eclipse.org/viewcvs/index.cgi/www/tm/development/scripts/lc?root=Eclipse_Website&view=co) (plus [lc.bat](http://dev.eclipse.org/viewcvs/index.cgi/www/tm/development/scripts/lc.bat?root=Eclipse_Website&view=co) if you are on Windows). Run it with one or more directories to count as arguments. **Example:**  
        `# Count lines in all RSE plugins, including empty lines   lc -e org.eclipse.rse.*`
    *   For contributions supplied as a patch, Eclipse 3.4 or later now has line counters directly in the "Apply patch" wizard. These can be used out of the box as-is -- they will count empty lines as well (which is less exact than the script here, but good enough for the "patch" case).
    *   As an alternative, you can use the [lcp script](http://dev.eclipse.org/viewcvs/index.cgi/www/tm/development/scripts/lcp?root=Eclipse_Website&view=co) (plus [lcp.bat](http://dev.eclipse.org/viewcvs/index.cgi/www/tm/development/scripts/lcp.bat?root=Eclipse_Website&view=co) if you are on Windows) which doesn't count empty lines in the patch. Run it with the patch file as argument. **Example:**  
        `# Count lines in contributed patch for bug 12345   lcp bug12345_patch.diff.txt`

Add a plugin or other project to the workspace
----------------------------------------------

*   Commit your modifications.
*   Add the **www-tm-development** project to your workspace. You can do this most easily by importing the Team Project set from
    *   [http://www.eclipse.org/tm/development/www-tm-development.psf](http://www.eclipse.org/tm/development/www-tm-development.psf)
*   When you already have the www-tm-development project in your workspace, **update it** before editing -- this saves you from having to merge before checkin.
*   In the **www-tm-development** project, update the **[team project sets](/tm/development/cvs_setup)**: You can either export a selection as team project set, and sort the lines afterwards (e.g. in Emacs, do M-X sort-lines). Or, edit the project set manually. For the pserver version, you typically need to replace-all ":extssh:" by ":pserver:".
*   Send an **E-mail** to tm-dev, informing everybody that team project sets have been updated with your new plugins.
*   For **release engineering** purposes, it may be necessary to also reference your new plugins in an existing feature, add new features, modify Mapfiles and/or the ANT files for nightly builds in the project org.eclipse.rse.build. These tasks can all be accomplished by your build and release engineer, so this is another reason for sending an **E-mail to tm-dev** when you added a new plugin.

Find missing or incorrect Copyright notices in my code and fix them
-------------------------------------------------------------------

*   Please use the Releng tool from the bottom of the [Eclipse 3.3 Download Page](http://archive.eclipse.org/eclipse/downloads/drops/R-3.3-200706251500/index.php#org.eclipse.releng)
*   See the description there at the end of the page: [Fix Copyright](http://archive.eclipse.org/eclipse/downloads/drops/R-3.3-200706251500/details.php#org.eclipse.releng)
*   For more detail information about "Fix Copyright" vs. "Advanced Fix Copyrights" also see
    *   [This message](http://dev.eclipse.org/mhonarc/lists/cross-project-issues-dev/msg01116.html) on cross-project-issues-dev
    *   [This message](http://dev.eclipse.org/mhonarc/lists/cross-project-issues-dev/msg01118.html) which was a reply on it
    *   [This message](http://dev.eclipse.org/mhonarc/lists/dsdp-tm-dev/msg01214.html) on dsdp-tm-dev
