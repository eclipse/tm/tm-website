---
title: "TM Committer Guidelines"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---
TM Committer Guidelines
=======================

While we do not want to get too process-heavy, there are a few simple things that will make all our lives easier. See also the [committer HOWTO](/tm/development/committer_howto) for simple cookbook-style instructions for common tasks.

Bugzilla Guidelines
-------------------

*   See the [Bug Process Page](/tm/development/bug_process) for bugzilla queries to find interesting bugs, and our general bug process (handling bugzilla states and priorities)
    
*   When you check in a fix for a bugzilla entry, **please** include the bugzilla number in the commit message. Example: **"\[139207\] Fix browsing into tar archives by dstore"**.
    
*   After committing a fix for bugzilla, set the entry **FIXED**. Do not set it closed until it has been tested and verified by somebody else.
    

Checkin Policies
----------------

*   When you add a new plugin, feature or other project to your workspace, please update the **Team Project Sets** so that other committers or CVS users can pick up your new project easily: Add the "www-tm-development" project to your workspace. You can do this most easily by importing the Team Project set from
    
    *   [http://www.eclipse.org/tm/development/www-tm-development.psf](http://www.eclipse.org/tm/development/www-tm-development.psf)
    
    Update the Project sets there and commit them to the web page:
    
    *   [http://www.eclipse.org/tm/development/rse-committer.psf](http://www.eclipse.org/tm/development/rse-committer.psf)
    *   [http://www.eclipse.org/tm/development/rse-anonymous.psf](http://www.eclipse.org/tm/development/rse-anonymous.psf)
    
    You can either export a selection as team project set, or edit the project set manually. For the pserver version, you typically need to replace-all ":extssh:" by ":pserver:".
    

IP Due Diligence
----------------

When you check in a contributed patch, we **have to follow IP Due Diligence guidelines**, as outlined on the [Eclipse IP Process Flowchart](http://www.eclipse.org/legal/EclipseLegalProcessPoster.pdf) and the [Project Log Guidelines](http://www.eclipse.org/projects/dev_process/project-log.php):

*   Check the contribution. In case of any uncertainty, consult the TM Lead, or Eclipse Legal:
    *   Does the contributor have sufficient **rights to contribute**? In other words, is it written by the contributor himself without referencing any 3rd party materials?
    *   Does the contribution include **anything not under EPL, or amount to > 250 lines of code** including all documentation etc.? If yes, a [Contribution Questionnaire](http://www.eclipse.org/legal/ContributionQuestionnairePart1-v1.0.php) has to be filled out! (The 250 line clause can be waived if the contributor is a committer himeself, and there is a member committer agreement in place for his employer company; it may also be waived for plain bug fixes).
    
*   Add the "www-tm-development" project to your workspace. You can do this most easily by importing the Team Project set from
    
    *   [http://www.eclipse.org/tm/development/www-tm-development.psf](http://www.eclipse.org/tm/development/www-tm-development.psf)
    
    In the www-tm project, add a line for the contribution in the **tm-log.csv** file. There are some sample lines already, so adding one should not be too hard.
    
*   Before committing both the code changes and the tm-log.csv, for any files patched by the contribution, add a **credit line for the contributor** into the file's copyright notice. It's good practice to give credit to our contributors. (when a whole new file is added by the contribution, leave the copyright notice as is, of course).
    
*   Make sure the bugzilla number is part of the commit message.
*   For more details, see the [Committer Howto on applying a patch from an external contributor](/tm/development/committer_howto#external_contrib)
.

Coding Guidelines
-----------------

We **do not want to be too restrictive right now** - especially do not rewrite existing code just to make it conform to some naming convention. Most of the RSE code seems to follow proper coding style already. In the end, we'll want to produce code and APIs of [Eclipse Quality](http://www.eclipse.org/projects/dev_process/eclipse-quality.php). Therefore, we better strive for quality early. Which means, try to follow common accepted guidelines for writing new code

Some useful references are

*   The [Eclipse Platform API Central](https://github.com/eclipse-platform/eclipse.platform/blob/master/docs/API_Central.md)
*   The [Eclipse Platform Devlopment Guidelines](http://wiki.eclipse.org/Development_Conventions_and_Guidelines)

Other stuff for reference
-------------------------

The [Eclipse Standard Charter](http://www.eclipse.org/projects/dev_process/Eclipse_Standard_TopLevel_Charter_v1.0.php) (as referenced by the [Tools Project Charter](http://www.eclipse.org/tools/eclipsetools-charter.php)) has more information about committer rights and duties, and our development process.

In particular, this charter says that committers need to agree on the [project plan](/tm/development/plan) and its modifications, and that committers can veto code changes.
