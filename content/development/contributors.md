---
title: "TM Project Contributors"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---

TM Project Contributors
=======================

### Target Management Lead

*   **Martin Oberhuber, Wind River Systems** (martin.oberhuber at windriver.com)  
    Martin is the overall project lead for the Target Management Project. As a software developer and architect, Martin is responsible for the current Target Manager component in Wind River Workbench.  
    Martin holds an MS degree in Telematics from the University of Technology Graz/Austria, and has been working on source code analysis and tools development since 1999.  
    [more about Martin Oberhuber](http://wiki.eclipse.org/Martin_Oberhuber)
*   **David Dykstal, IBM** (david\_dykstal at us.ibm.com)  
    David is the Project Lead for the IBM Remote System Explorer, and thus plays a key role in all activities around RSE.  
    David has worked either for or with IBM for over 28 years, including a significant stint with Object Technology International. Dave has worked on compilers, user interfaces, database tools, and IDE's.

### Committers (alphabetic order by last name)

*   **Felix Burton**, Wind River
*   **Xuan Chen**, IBM
*   **Kevin Doyle**, IBM
*   **Anna Dushistova**, MontaVista
*   **David Dykstal**, IBM
*   **Radoslav Gerganov**, ProSyst
*   **David McKnight**, IBM
*   **Martin Oberhuber**, Wind River
*   **Michael Scharf**, Wind River
*   **Uwe Stieber**, Wind River
*   **Eugene Tarassov**, Wind River

### Former Committers (alphabetic order by last name)

*   **Javier Montalvo-Orus**, Symbian
*   **Kushal Munir**, IBM
*   **Ted Williams**, Wind River

### Contributors (in alphabetical order)

This is the list of people who have made code contributions to the TM Project on Bugzilla so far, with their code actually being merged into the TM codebase in CVS or SVN. See the [TM IP Log](http://www.eclipse.org/projects/ip_log.php?projectid=tools.tm) for more details about individual contributions. The list below is sorted alphabetically by last name:

*   **Sean Adams**, Cisco
*   **Peder Andersen**, Wind River
*   **Michael Berger**, IBM
*   **Nick Boldt**, Red Hat
*   **John Cortell**, Freescale
*   **Li Ding**, IBM
*   **Johann Draschwandtner**, Wind River
*   **Sheldon D Souza**, Celunite
*   **Daniel Friederich**, Freescale
*   **Jacob Garcowski**, IBM
*   **Oyvind Harboe**, Zylin
*   **Rob Hasselbaum**, BEZ Systems, Inc
*   **Bryan Hunt**
*   **Kenya Ishimoto**, IBM Japan
*   **Mikhail Kalugin**, Xored Software
*   **Sidharth Kodikal**, Freescale
*   **Mike Kucera**, IBM
*   **Krzysztof Kosmatka**, Atrem S.A.
*   **Yu-Fen Kuo**, MontaVista
*   **Michael Sills-Lavoie**, Ecole Polytechnique de Montreal
*   **Anton Leherbauer**, Wind River
*   **Kit Lo**, IBM
*   **Laura Le Padellec**, Wind River
*   **Renan Le Padellec**, Wind River
*   **Justin Lin**, IBM
*   **Vlad Lungu**, Wind River
*   **Johnson Ma**, Wind River
*   **Rupen Mardirossian**, IBM
*   **Ewa Matejska**, ACCESS Systems (formerly PalmSource)
*   **Willian Mitsuda**, (private)
*   **Gaetan Morice**, Anyware-Tech
*   **Benjamin Muskalla**, Innoopract
*   **Masao Nishimoto**, IBM
*   **Alex Panchenko**, Xored Software
*   **Philippe Proulx**, Ecole Polytechnique de Montreal
*   **Mirko Raner**
*   **Zhou Renjian**, Shanghai Kortide
*   **Ken Ryall**, Nokia
*   **Tobias Schwarz**, Wind River
*   **David Sciamma**, Anyware-Tech
*   **Timur Shipilov**, Xored Software
*   **Nikita Shulga**, Mentor Graphics
*   **Andrei Sobolev**, Xored Software
*   **Max Stepanov**, Appcelerator
*   **Ed Swartz**, Nokia
*   **Remy Chi Jian Suen**, IBM
*   **Ruslan Sychev**, Xored Software
*   **Noriaki Takatsu**, IBM
*   **Patrick Tasse**, Ericsson
*   **Olivier Thomann**, IBM
*   **Peter Wang**, IBM
*   **Greg Watson**, IBM
*   **Lothar Werzinger**, Tradescape Inc.
*   **Don Yantzi**, IBM
*   **Richie Yu**, IBM

### Other interested parties

We also honor the contributions that people make in the form of bug reports, good questions on the mailing list, or other discussions. These are partially reflected in the following

*   **[Bugzilla Report](https://bugs.eclipse.org/bugs/report.cgi?x_axis_field=resolution&y_axis_field=reporter&z_axis_field=&query_format=report-table&product=Target+Management&format=table&action=wrap&negate0=1&field0-0-0=reporter&type0-0-0=equals&value0-0-0=martin.oberhuber%40windriver.com&field0-0-1=reporter&type0-0-1=equals&value0-0-1=Javier.MontalvoOrus%40gmail.com&field0-0-2=reporter&type0-0-2=equals&value0-0-2=david_dykstal%40us.ibm.com&field0-0-3=reporter&type0-0-3=equals&value0-0-3=dmcknigh%40ca.ibm.com&field0-0-4=reporter&type0-0-4=equals&value0-0-4=kjdoyle%40ca.ibm.com&field0-0-5=reporter&type0-0-5=equals&value0-0-5=Michael.Scharf%40windriver.com&field0-0-6=reporter&type0-0-6=equals&value0-0-6=eugene.tarassov%40windriver.com&field0-0-7=reporter&type0-0-7=equals&value0-0-7=uwe.stieber%40windriver.com&field0-0-8=reporter&type0-0-8=equals&value0-0-8=xuanchen%40ca.ibm.com&field0-0-9=reporter&type0-0-9=equals&value0-0-9=r_gerganov%40prosyst.bg&field0-0-10=reporter&type0-0-10=equals&value0-0-10=felix.burton%40windriver.com&field0-0-11=reporter&type0-0-11=equals&value0-0-11=ted.williams%40windriver.com&field0-0-12=reporter&type0-0-12=equals&value0-0-12=kmunir%40ca.ibm.com)** - all bug reporters except committers

Apart from plain bugs, here is the list of companies and people who have been active in the TM face-to-face meetings and phone conferences so far, or made interesting contributions on the TM Mailing List. The list is sorted alphabetically by company first, then by last name:

*   **Accelerated Technology**: Mark Bozeman, George Clark, Aaron Spear
*   **Cisco**: Hemang Lavana
*   **Curtiss-Wright Controls**: Mark Littlefield, Darian Wong
*   **Digi**: John Leier
*   **FreeScale**: Kirk Beitz, John Cortell, Daymon Rogers
*   **IBM**: Alan Boxall, Denise Schmidt
*   **IBM / LANL**: Greg Watson
*   **Intel**: Peter Lachner
*   **Montavista**: Joe Green, Pierre-Alexandre Masse
*   **QNX**: Alex Chapiro, David Inglis, Doug Schaefer
*   **Star Bridge Systems**: Matthew Scarpino
*   **Symbian**: Victor Palau, Neil Taylor
*   **TI**: Paul Gingrich
*   **TU Munich**: Tianchao Li
*   **Wind River**: Martin Gutschelhofer, Brian Nettleton
*   **Zend Technologies**: Yossi Leon, Yaron Mazor

