---
title: "TM Repository and Team Project Sets"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---

TM Repository and Team Project Sets
===================================

When you want to work on RSE code obtained from our CVS Repositories, the simplest way to set up your workspace is to import our team project sets.

Simply pick the project set below that is appropriate for your use, save it to a local disk and in Eclipse choose **File > Import > Team > Team Project Set**.

For most contributors, we recommend using [**rse-anonymous.psf**]({{< resource url="development/rse-anonymous.psf" >}}). Since the RSE code is located on three different repositories, importing this project set will ask you three times for your user and password information. Use

*   User: **anonymous**
*   Password: **your.email@your.provider**

After entering this three times during import, Eclipse will not ask you again. Since committers with write access to the repository need a different repository setup, there are specific project sets for committers.

Once you have obtained a project set, a project named **www-tm-development** will also be part of your workspace. This project holds the web pages you are just reading, including the team project sets themselves. So when you want to update your workspace e.g. because you have been informed that new plugins have been added, just synchronize the www-tm-development project with the repository, and then import the proper team project set from your local workspace (there is no need to go back to the web page again).

For more details on Team Project Sets, see the [Eclipse Workbench documentation.](http://help.eclipse.org/help32/index.jsp?topic=/org.eclipse.platform.doc.user/tasks/tasks-cvs-project-set.htm)

| Project Set Contents    | Anonymous Access | Committer Access |
| ----------------------- | ---------------- | ---------------- |
| Core RSE plugins, including examples, docs and all subsystem implementations | [rse-anonymous.psf]({{< resource url="development/rse-anonymous.psf" >}}) | [rse-committer.psf]({{< resource url="development/rse-committer.psf" >}}) |
| RSE feature projects, tests and release engineering support projects | [rse-releng-anonymous.psf]({{< resource url="development/rse-releng-anonymous.psf" >}}) | [rse-releng-committer.psf]({{< resource url="development/rse-releng-committer.psf" >}}) |
| Discovery component (archived) | [discovery-anonymous.psf]({{< resource url="development/discovery-anonymous.psf" >}}) | [discovery-committer.psf]({{< resource url="development/discovery-committer.psf" >}}) |
| Terminal component | [terminal-anonymous.psf]({{< resource url="development/terminal-anonymous.psf" >}}) | [terminal-committer.psf]({{< resource url="development/terminal-committer.psf" >}}) |
| Complete workspace, including all four project sets above | [tm-all-anonymous.psf]({{< resource url="development/tm-all-anonymous.psf" >}}) | [tm-all-committer.psf]({{< resource url="development/tm-all-committer.psf" >}}) |
| TM Orbit contribution (Jakarta ORO and Commons Net) | [tm-orbit-anonymous.psf]({{< resource url="development/tm-orbit-anonymous.psf" >}}) | [tm-orbit-committer.psf]({{< resource url="development/tm-orbit-committer.psf" >}}) |

WWW viewcvs access
------------------

Our repositories can also be accessed over the internet through the viewcvs interface:

*   [TM Core Repository](http://dev.eclipse.org/viewcvs/index.cgi/org.eclipse.tm.core/?cvsroot=Tools_Project), holding re-usable core components without further dependencies like third party libraries (Apache Commons Net, Jakarta-ORO), Discovery components and Terminal view.
*   [RSE Repository](http://dev.eclipse.org/viewcvs/index.cgi/org.eclipse.tm.rse/?cvsroot=Tools_Project), holding the Remote System Explorer (RSE) framework including services, subsystems, UI components, examples, tests, documentation and all release engineering support projects.
*   [Orbit Repository](http://dev.eclipse.org/viewcvs/index.cgi/org.eclipse.orbit/?cvsroot=Tools_Project), holding the TM contributions to Orbit, namely bundled versions of Apache Commons Net 1.4.1 and Jakarta ORO 2.0.8.
*   [TM Website Repository](http://dev.eclipse.org/viewcvs/index.cgi/www/tm/?cvsroot=Eclipse_Website), holding the project web pages.

Direct manual access to our Repositories
----------------------------------------

Finally, if you want to manually browse our repositories in the Eclipse CVS Repository Explorer or a similar CVS browser, here is how to access them:

|   | Repository | Module path |
| - | ---------- | ----------- |
| TM Core Repository | :pserver:dev.eclipse.org:/cvsroot/tools | org.eclipse.tm.core |
| RSE Main Repository | :pserver:dev.eclipse.org:/cvsroot/tools | org.eclipse.tm.rse |
| Orbit Repository | :pserver:dev.eclipse.org:/cvsroot/tools | org.eclipse.orbit |
| TM Website Repository | :pserver:dev.eclipse.org:/cvsroot/org.eclipse | www/tm |

Change Logs
-----------

The TM project maintains change logs for the latest modifications to the repositories. Unfortunately, these need to be separate for the Core and RSE repositories due to technical constraints:

*   [RSE Main Changelog](http://download.eclipse.org/tm/downloads/drops/N-changelog/index.html)
*   [Core Components Changelog](http://download.eclipse.org/tm/downloads/drops/N-changelog/core/index.html)

Commit Notifications
--------------------

You can subscribe to the [tm-cvs-commit](https://dev.eclipse.org/mailman/listinfo/tm-cvs-commit) mailing list to receive notification E-Mails for new checkins.

Background information on CVS and Eclipse
-----------------------------------------

*   See [Using Eclipse with CVS](http://wiki.eclipse.org/CVS_Howto) for instructions on how to use Eclipse with CVS.
