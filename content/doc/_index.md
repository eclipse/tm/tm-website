---
title: "TM Developer Documents"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---
TM Developer Documents
======================

Use Cases (your best bet to get started!)
-----------------------------------------

*   [TM Use Cases v1.1c.pdf](/tm/doc/TM_Use_Cases_v1.1c.pdf)
*   [nucleus edge target usecase.pdf](/tm/doc/nucleus_edge_target_usecase.pdf)

Current API Docs
----------------

These are part of the downloaded SDK, and available online. The online docs are automatically generated from the latest code every night.

*   [RSE User Guide](http://help.eclipse.org/indigo/topic/org.eclipse.rse.doc.user/gettingstarted/g_start.html)
*   [RSE Developer Guide](http://help.eclipse.org/indigo/topic/org.eclipse.rse.doc.isv/guide/rse_int.html)
*   [DStore Developer Guide](http://help.eclipse.org/indigo/topic/org.eclipse.dstore.doc.isv/toc.html)

Design Documents
----------------

*   [TM Overview (ppt)](/tm/meetingnotes/ff01_chicago/DSDPTM_Overview.ppt)
*   [TM Design Proposal v0.3.pdf](/tm/doc/DSDPTM_Design_Proposal_v0.3.pdf)
*   [TM Target Connection Adapter Proposal.pdf](/tm/doc/DSDPTM_Target_Connection_Adapter_Proposal.pdf)
*   [Brainstorming Notes from Chicago Meeting](/tm/meetingnotes/ff01_chicago/DSDPTM_Brainstorming_2005-10-14.htm)

Presentations
-------------

*   [TM 3.0 Release Review Slides](http://www.eclipse.org/project-slides/TM_3.0_Release_Review.pdf)
*   [TM 2.0 Release Review Slides (PPT: 237k)](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_2.0_Release_Review.ppt) | [(PDF: 300K)](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_2.0_Release_Review.pdf)
*   Eclipse Summit Europe 2006-9-29 [TM Overview Slides (PPT: 314k)](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.ppt) | [(PDF: 298k)](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.pdf)
*   [TM 1.0 Release Review Slides (PPT: 248k)](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_1.0_Release_Review_v3.ppt) | [(PDF: 195K)](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_1.0_Release_Review_v3a.pdf)
*   EclipseCon 2006: [Using and Extending the DSDP Target Management Framework (PPT: 850k)](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-3-22_EclipseCon_Target_Management.ppt) | [(Zipped PPT: 559k)](http://eclipsezilla.eclipsecon.org/php/attachment.php?bugid=287)

Older Meeting Notes (Newer notes are on the [Wiki](https://gitlab.eclipse.org/groups/eclipse/tm/-/wikis/TM))
-------------------------------------------------------------------------------

#### CDT Conference Ottawa, 2005 October 25-26

*   [DSDP Update at CDT Conference (ppt)](/tm/conferences/2005_CDT_Conference_DSDP_Update.ppt)

#### F2F Meeting Chicago, 2005 October 12-13

*   [Meeting Notes](/tm/meetingnotes/ff01_chicago/2005-10-12_Chicago_TM_MeetingNotes.htm)
*   [Brainstorming Notes](/tm/meetingnotes/ff01_chicago/DSDPTM_Brainstorming_2005-10-14.htm)
*   [Eclipse IP Due Diligence (ppt)](/tm/meetingnotes/ff01_chicago/Eclipse_IP_Due_Diligence.ppt)
*   [TM Overview (ppt)](/tm/meetingnotes/ff01_chicago/DSDPTM_Overview.ppt)
*   [On the Borderline between Target Management and Debugging (ppt)](/tm/meetingnotes/ff01_chicago/DSDPTM_DD_Borderline.ppt)
*   [WR Debugger Backend Interface Proposal (ppt)](/tm/meetingnotes/ff01_chicago/DSDPTM_DebuggerBackend.ppt)
*   [Target Connection Adapter Proposal (ppt)](/tm/meetingnotes/ff01_chicago/DSDPTM_TCA.ppt)

#### Phone Meeting Notes

*   [2005-11-07 Phone Conference](/tm/meetingnotes/DSDP-TM_MeetingNotes_20051107.htm)
*   [2005-09-26 Phone Conference](/tm/meetingnotes/DSDP-TM_MeetingNotes_20050926.pdf)  
    
*   [2005-08-29 Phone Conference](/tm/meetingnotes/DSDP-TM_MeetingNotes_20050829.pdf)  
    
*   [2005-08-08 Phone Conference](/tm/meetingnotes/DSDP-TM_MeetingNotes_20050808.pdf)  
    
*   [2005-07-18 Phone Conference](/tm/meetingnotes/DSDP-TM_MeetingNotes_20050718.pdf)  
    
*   [2005-06-27 Phone Conference](/tm/meetingnotes/DSDP-TM_MeetingNotes_20050627.pdf)  
    
*   [2005-06-13 Phone Conference](/tm/meetingnotes/DSDP-TM_MeetingNotes_20050613.pdf)  
    