---
title: "TM Downloads"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---
TM Downloads
============

All downloads are provided under the terms and conditions of the [Eclipse Foundation Software User Agreement](https://www.eclipse.org/legal/epl/notice/) unless otherwise specified.

Current releases and CI builds
------------------------------

#### Latest CI Builds

Compatible with Eclipse 4.12 (Simrel 2019-06)

*   Terminal & RSE 4.5.101: [CI build](https://ci.eclipse.org/tm/view/terminal/job/tm-terminal-rse_master/)

  

#### TM Terminal & RSE 4.5.100

Compatible with Eclipse 4.11 (Simrel 2019-03)

*   [update site](http://download.eclipse.org/tm/updates/4.5.100-SNAPSHOT/repository/) or [repo zip](http://download.eclipse.org/tm/updates/4.5.100-SNAPSHOT/tm-repository-4.5.100-SNAPSHOT.zip)

* * *

Old Releases
------------

#### TM Terminal & RSE 4.5.1

Compatible with Eclipse 4.10 (Simrel 2018-12)

*   [update site](http://download.eclipse.org/tm/updates/4.5.1-SNAPSHOT/repository/) or [repo zip](http://download.eclipse.org/tm/updates/4.5.1-SNAPSHOT/tm-repository-4.5.1-SNAPSHOT.zip)

#### TM Terminal & RSE 4.5.0

Compatible with Eclipse 4.9 (Simrel 2018-09)

*   [update site](http://download.eclipse.org/tm/updates/4.5.0/repository/) or [repo zip](http://download.eclipse.org/tm/updates/4.5.0/tm-repository-4.5.0.zip)

#### TM Terminal 4.4 and RSE 3.7.100

Compatible with Eclipse 4.8 (Photon)

*   Terminal 4.4: [update site](http://download.eclipse.org/tm/terminal/updates/4.4milestones/20180611/) or [repo zip](http://download.eclipse.org/tm/terminal/updates/4.4milestones/20180611/org.eclipse.tm.terminal.repo.zip)
*   RSE 3.7.100: [update site](http://download.eclipse.org/tm/updates/3.7.100/repository/) or [repo zip](http://download.eclipse.org/tm/updates/3.7.100/rse-repository-3.7.100.zip)

  

#### TM Terminal 4.3 and RSE 3.7.3

Compatible with Eclipse 4.7.3 (Oxygen.3a)

*   Terminal 4.3: [update site](http://download.eclipse.org/tm/terminal/updates/4.3/GA/) or [repo zip](http://download.eclipse.org/tm/terminal/updates/4.3/GA/org.eclipse.tm.terminal.repo.zip)
*   RSE 3.7.3: [update site](http://download.eclipse.org/tm/updates/4.3milestones/20170425/) or [repo zip](http://download.eclipse.org/tm/updates/4.3milestones/20170425/org.eclipse.tm.repo.zip)

  

#### TM 4.0 (Mars)

Compatible with Eclipse 3.8.2 and later. See the [Release Review](https://projects.eclipse.org/projects/tools.tm/reviews/4.0.0-release-review).

*   Terminal [Marketplace](http://marketplace.eclipse.org/content/tm-terminal) | [p2 update site](http://download.eclipse.org/tm/terminal/updates/4.0) | [org.eclipse.tm.terminal-4.0.0.zip](/downloads/download.php?file=/tm/terminal/updates/4.0/org.eclipse.tm.terminal-4.0.0.zip) (1.4 MiB)
*   RSE p2 software repository: [http://download.eclipse.org/tm/updates/4.0](http://download.eclipse.org/tm/updates/4.0)

### Archived Releases

Older releases are found on

*   [The TM Download Site](http://download.eclipse.org/tm/downloads/)
*   [The TM Archive Site](http://archive.eclipse.org/tm/downloads/)
*   The TM Update Sites:
    *   http://archive.eclipse.org/tm/updates/R3.x/3.7 or [repo zip](http://archive.eclipse.org/tm/updates/R3.x/3.7/GA/org.eclipse.tm.repo.zip) (Luna SR2)
    *   http://archive.eclipse.org/tm/updates/R3.x/3.6 (Luna SR1)
    *   http://archive.eclipse.org/tm/updates/R3.x/3.5 (Kepler)
    *   http://archive.eclipse.org/tm/updates/R3.x/3.4 (Juno)
    *   http://archive.eclipse.org/tm/updates/R3.x/3.3 (Indigo)
    *   http://archive.eclipse.org/tm/updates/R3.x/3.2 (Helios)
    *   http://archive.eclipse.org/tm/updates/R3.x/3.1 (Galileo)
    *   http://archive.eclipse.org/tm/updates/R3.x/3.0 (Ganymede)
*   Legacy: [TM 2.x update site](http://download.eclipse.org/tm/updates/2.0))

Older downloads have been submitted to eclipse.org via bugzilla:

*   Bugzilla [65471](https://bugs.eclipse.org/bugs/show_bug.cgi?id=65471): Remote System Framework (RSF) 2.0.0 code and [presentation](https://bugs.eclipse.org/bugs/attachment.cgi?id=18820)

In addition to that, [Developer Documents](/tm/doc/) are available for download.