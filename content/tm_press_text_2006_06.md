---
title: "Press Text: Target Management - Jun 2006"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---

Press Text: Target Management - Jun 2006
========================================

Target Management Project
-------------------------

The Target Management Project is creating frameworks and a consistent UI for accessing remote compute resources from Eclipse. The current code base (Remote Systems Explorer, RSE) supports remote file, shell and process access through a supplied Java based agent, or standard protocols like FTP and secure shell (ssh). Adopters are currently writing extensions to bring their own proprietary protocols into the framework for accessing devices like hardware debuggers. A 1.0 release is planned for October 20, a functional complete milestone (M3) is planned for June 30. A 2.0 release shall be aligned with Eclipse 3.3 in June 2007.

For the 2.0 release planning and the longer term future, the TM project participants are dealing with advanced extensions to the framework in various technology sub-groups (see [https://gitlab.eclipse.org/groups/eclipse/tm/-/wikis/TM](https://gitlab.eclipse.org/groups/eclipse/tm/-/wikis/TM)). Active discussions are also going on for collaboration with other Eclipse Projects, especially the [CDT project](http://www.eclipse.org/cdt), the [Eclipse Communication Framework (ECF)](http://www.eclipse.org/ecf), the [Parallel Tools Platform (PTP)](http://www.eclipse.org/ptp), and the [Eclipse Platform/Team group.](http://dev.eclipse.org/viewcvs/index.cgi/%7Echeckout%7E/platform-vcm-home/main.html) A comprehensive [document outlining the use-cases](http://www.eclipse.org/tm/doc/TM_Use_Cases_v1.1c.pdf) and applicability of the Target Management Project is available from the "Getting Started" area on the Project Website at [http://www.eclipse.org/tm](http://www.eclipse.org/tm).

#### active project participants:

*   IBM - 3 committers + 1 contributor, core RSE development
*   Windriver - 1 committer, contributed ssh services, lots of testing, project lead and communications
*   PalmSource - contributed CDT Launch
*   Symbian - bugfixes, about to contribute zeroconf-discovery
*   PTP Project/LANL - testing on Macintosh, EFS, ssh

#### active adopters:

*   Freescale - writing proprietary wizards for JTAG on RSE
*   Accelerated Technology - about to write proprietary Services for JTAG on RSE
*   Montavista
*   Siemens

#### features committed to CVS:

*   Remote file system access through ssh, ftp or dstore protocols
*   Remote shell (command) access through ssh or dstore protocols
*   Remote process access through dstore protocol on Linux
*   dstore protocol implementation and agent for remote search, remote archive exploring, remote process and other pluggable miners
*   CDT Remote Launch Integration Example
*   Common UI - Single consistent view, including filters
*   Team sharing of connection and filter definitions
*   Framework for plugging in custom protocols, services and subsystems

#### what features are coming next:

*   user-defined menus and actions for instant access to remote programs
*   import/export wizards for fast remote file transfer
*   Apache Commons Net integration for FTP, Telnet
*   Bring documentation/examples up-to-date

#### release plans:

*   1.0 release planned for October 20
    *   M3 milestone (June 30) to be feature complete
*   2.0 release planned with Eclipse 3.3 June 2007

#### recruiting efforts:

*   A lot of companies is already active on the TM project, we continue to lower the bar for using and extending the RSE
*   Communications with other Eclipse projects, e.g. Platform/Team, TPTP and WTP. They all have some need for remote system access.
*   RSE ssh tools will be useful for every Eclipse committer, we want to give simple access through an update site (and announce publicly among all committers) our M3 milestone end of june

#### quotation from the project lead:

"The real benefit of RSE is that it gathers lots of totally heterogeneous systems and subsystems under a single consistent view."
