---
title: "Target Management Home"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---

Target Management Home
======================

Mission Statement
-----------------

The Target Management project creates data models and frameworks to configure and manage remote systems (from mainframe to embedded), their connections, and their services.

### The Vision

To be the Eclipse _"Explorer of the Network Neighborhood"_, with pluggable information providers under a single, consistent UI. Interactively discover, drill down, analyze remote systems (from mainframes down to embedded systems), and provide the context for more advanced actions being plugged in to it.

### The Toolkit

The [TM Terminal](http://marketplace.eclipse.org/content/tm-terminal) has received excellent reviews and star ratings on the Eclipse Marketplace. It provides a small, re-usable component for Terminal emulation and remote access.

The [Remote System Explorer (RSE)](https://tmober.blogspot.com/2006/11/remote-system-explorer-10-is-released.html) framework integrates any sort of heterogeneous remote resources through a concept of pluggable subsystems. The base toolkit includes a Remote Files subsystem that allows [transparent working on remote computers](https://eclipsewebmaster.blogspot.com/2007/01/remote-editing-using-eclipse.html) just like the local one, a shell and a processes subsystem. RSE is in maintenance mode, with possible successors being the [TCF Target Explorer](/tcf) or a new framework based on the [org.eclipse.remote API](https://projects.eclipse.org/projects/tools.tm/reviews/4.0.0-release-review).

Vendors are extending the RSE with custom subsystems for debugging, remote VNC display and other uses.

### Releases

Our latest full release is TM 4.4, but only the Terminal received significant updates. RSE is still in maintenance mode and remains at version 3.7.100 (updates for Photon). See also the [downloads page](/tm/downloads).

### Quick Links

*   [**Wiki**](https://gitlab.eclipse.org/groups/eclipse/tm/-/wikis/TM) | We use the Wiki extensively for collaboration. Find ongoing discussions, meeting notes and other "not so official" stuff there.
*   [**Newsgroup**](news://news.eclipse.org/eclipse.tm) | For general questions and community discussion ([Web access](http://www.eclipse.org/newsportal/thread.php?group=eclipse.tm), [archive](http://dev.eclipse.org/newslists/news.eclipse.tm/maillist.html)).
*   [**Mailing List**](http://dev.eclipse.org/mailman/listinfo/tm-dev) | For project development discussions.
*   [**Bugs**](/tm/development/bug_process) | View [all open](https://gitlab.eclipse.org/eclipse/tm/org.eclipse.tm/-/issues) issues | [Submit new](https://gitlab.eclipse.org/eclipse/tm/org.eclipse.tm/-/issues/new) bugs 
*   [**Use cases**](/tm/doc/TM_Use_Cases_v1.1c.pdf) and requirements for Target Management
*   [**Architectural Overview**](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.pdf) ([PPT](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.ppt) | [PDF](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.pdf)).
*   [**TM Project Plans**](/tm/development/plan)

### Events

*   Monthly developer phone conference, every 1st wednesday of the month, 9am PST (See the [Wiki](https://gitlab.eclipse.org/groups/eclipse/tm/-/wikis/TM) for actual agenda and details)
*   **March 17-20, 2008**: [EclipseCon 2008](http://www.eclipsecon.org/2008) -
    *   [**Remote access with the Target Management Project**](http://www.eclipsecon.org/2008/?page=sub/&id=38), Tutorial by Martin Oberhuber (Wind River) (slides: [PPT](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Tutorial_ECon08.ppt) 757 KB | [PDF](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Tutorial_ECon08.pdf) 639 KB) | (code: [tcf-0.2.0.zip](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/tcf-0.2.0.zip) 3.7 MB | [tmtutorial.zip](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Tutorial_Econ08_samples.zip) 465 KB)
    *   [**Target Management New and Noteworthy**](http://www.eclipsecon.org/2008/?page=sub/&id=39), Short Talk by Martin Oberhuber (Wind River), TM project lead (slides: [PPT](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Short_ECon08.ppt) 707 KB | [PDF](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Short_ECon08.pdf) 581 KB)
*   **October 9-11, 2007**: Eclipse Summit Europe 2007 -
    *   [**The Target Management Project**](http://www.eclipsecon.org/summiteurope2007/index.php?page=detail/&id=21), long talk by Martin Oberhuber (slides: [PPT](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2007-10-10_TM_ESE2007.ppt) | [PDF](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2007-10-10_TM_ESE2007.pdf))
*   **April 12, 2007**: [Webinar](http://live.eclipse.org/node/229): TM goals, architecture, future plans and online demo ([50 minute full recording](http://live.eclipse.org/node/229) | [PPT slides](http://www.eclipse.org/projects/slides/TM_Webinar_Slides_070412.ppt))
*   **Sept. 27, 2006**: TM passed its 1.0 Release Review. The Slides are an interesting read for everyone (Slides as [PPT](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_1.0_Release_Review_v3.ppt) | [PDF](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_1.0_Release_Review_v3a.pdf)).

### News

Sept. 2018: [TM & RSE 4.5](/tm/downloads) are released with Eclipse Simrel 2018-09.

### Terminal Restructuring

April 25, 2014: TM Terminal has been restructured and is now on the [Marketplace](http://marketplace.eclipse.org/content/tm-terminal).

### Getting Started

*   [TM Getting Started](/tm/tutorial/)
*   [TM Overview Slides](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.pdf)
*   [TM Webinar](http://live.eclipse.org/node/229)
*   [TM Use Cases](/tm/doc/TM_Use_Cases_v1.1c.pdf)
*   [TM Project Plan](/tm/development/plan)
*   [Press text - June 2006](/tm/tm_press_text_2006_06)

