---
title: "Getting Started with Target Management"
hide_page_title: true
sidebar: 
    - home
    - users
    - integrators
    - contributors
---
Getting Started with Target Management
======================================

The Target Management project creates data models and frameworks to configure and manage remote systems, their connections, and their services.

Our first deliverable is the _Remote System Explorer (RSE)_, a framework and toolkit in Eclipse Workbench, that allows you to connect and work with a variety of remote systems, including

*   **remote file systems** through SSH, FTP or dstore agents (seamless editing of remote files including remote search and compare),
*   **remote shell access** (compiling with error navigation),
*   **remote process** handling through dstore agents,
*   and **remote debugging** through CDT / gdb / gdbserver.

Besides that, we are working on **flexible, re-usable components** for Networking and Target Management that run integrated or without RSE. The following are available from the TM 3.0 download pages already:

*   An ANSI / vt102 compatible **Terminal** widget with pluggable Serial, ssh and Telnet connectors (requires Platform now but can be ported to RCP / J2ME)
*   Apache **Commons Net** re-bundled for Eclipse to support FTP, rlogin, telnet and other standard protocols (requires J2SE-1.2 only)

TM downloads are available from our [download site](/tm/downloads). The best resource for programmers to get started with the framework is the [EclipseCon 2008 Tutorial](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Tutorial_ECon08.pdf) (includes [sample code](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Tutorial_Econ08_samples.zip)), which is based on TM 3.0M5. Another [Tutorial](http://help.eclipse.org/indigo/index.jsp?topic=/org.eclipse.rse.doc.user/gettingstarted/g_start.html) is available as part of the ISV documentation, and an [FAQ](https://gitlab.eclipse.org/groups/eclipse/tm/-/wikis/TM_and_RSE_FAQ) is available on the project Wiki.

Our plans beyond TM 3.0 are available from the Target Management [Project Plan](/tm/development/plan) and our [Use Cases Document](/tm/doc/TM_Use_Cases_v1.1c.pdf), which covers all areas of interest to us.

### For more information, see the

*   [Target Management Overview Slides](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.pdf) which include a diagram of the envisioned components and architecture for our project ([PPT](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.ppt) | [PDF](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.pdf)).
*   [**Target Management 3.0 New & Noteworthy**](http://www.eclipsecon.org/2008/?page=sub/&id=39), EclipseCon 2008 ([PPT](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Short_ECon08.ppt) 707 KB | [PDF](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Short_ECon08.pdf) 581 KB)
*   [TM 2.0 Webinar](http://live.eclipse.org/node/229): goals, architecture, future plans and online demo ([50 minute full recording](http://live.eclipse.org/node/229) | [PPT slides](http://www.eclipse.org/projects/slides/TM_Webinar_Slides_070412.ppt))
*   [TM and RSE FAQ](https://gitlab.eclipse.org/groups/eclipse/tm/-/wikis/TM_and_RSE_FAQ)
*   EclipseCon 2008 [**TM 3.0 Tutorial**](http://www.eclipsecon.org/2008/?page=sub/&id=38) (includes [slides](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Tutorial_ECon08.pdf) and [sample code](http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Tutorial_Econ08_samples.zip))
*   [TM Online Docs Tutorial](http://help.eclipse.org/indigo/index.jsp?topic=/org.eclipse.rse.doc.user/gettingstarted/g_start.html)
*   [TM 3.0 Known Issues and Workarounds](https://gitlab.eclipse.org/groups/eclipse/tm/-/wikis/TM_3.0_Known_Issues_and_Workarounds)
*   [Target Management Use-Case Document](/tm/doc/TM_Use_Cases_v1.1c.pdf) to understand what scenarios we want to cover with our project.
*   [Target Management Project Plan](/tm/development/plan) to understand what features and releases are coming next.
