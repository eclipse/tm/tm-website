---
title: Featured Story in Jumbotron
headline: Featured Story
tagline: It can also appear in the jumbotron...
show_featured_story: true
layout: single
---

Featured story can also appear in the jumbotron instead of the main content of a webpage.
