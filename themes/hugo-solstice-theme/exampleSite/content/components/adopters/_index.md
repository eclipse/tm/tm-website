---
title: Adopters
description: Adopters component
---

Renders a list of projects and their adopters.

```md
{{</* eclipsefdn_adopters */>}}
```

{{< eclipsefdn_adopters >}}

## Parameters

### List projects and adopters for a specific working group

The `working_group` parameter allows you to specify a working group.

#### Examples
